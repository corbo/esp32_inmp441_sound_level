// https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/system/freertos.html

#include <Arduino.h>
#include <WiFi.h>
#include <PubSubClient.h>
#include <Wire.h>
#include "slm.h"
#include <config.h>

// LED Pin
const int ledPin = 22;
//
// I2S pins - Can be routed to almost any (unused) ESP32 pin.
//            SD can be any pin, inlcuding input only pins (36-39).
//            SCK (i.e. BCLK) and WS (i.e. L/R CLK) must be output capable pins
//
// Below ones are just example for my board layout, put here the pins you will use
//
// #define I2S_WS            15   Grey    TDO
// #define I2S_SCK           2    White   2
// #define I2S_SD            13   Brown   TCK

WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
int value = 0;
#define MQTT_TASK_PRI 4
#define MQTT_TASK_STACK 10000

void task_slm(){
  samples_queue = xQueueCreate(8, sizeof(sum_queue_t));  
  xTaskCreate(mic_i2s_reader_task, "Mic I2S Reader", I2S_TASK_STACK, NULL, I2S_TASK_PRI, NULL);
}

void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* message, unsigned int length) {
  uint64_t chip_id = ESP.getEfuseMac();
  uint16_t chip = (uint16_t)(chip_id >> 32);
  char subscribed[32];

  snprintf(subscribed, 32, "ESP-%04X/output", chip);
 Serial.print("Message arrived on topic: ");
  Serial.print(topic);
  Serial.print(". Message: ");
  String messageTemp;
  
  for (int i = 0; i < length; i++) {
    Serial.print((char)message[i]);
    messageTemp += (char)message[i];
  }
  Serial.println();

  // Feel free to add more if statements to control more GPIOs with MQTT

  // If a message is received on the topic esp32/output, you check if the message is either "on" or "off". 
  // Changes the output state according to the message
  if (String(topic) == String(subscribed)) {
    Serial.print("Changing output to ");
    if(messageTemp == "on"){
      Serial.println("on");
      digitalWrite(ledPin, HIGH);
    }
    else if(messageTemp == "off"){
      Serial.println("off");
      digitalWrite(ledPin, LOW);
    }
  }
}

void reconnect() {
  uint64_t chip_id = ESP.getEfuseMac();
  uint16_t chip = (uint16_t)(chip_id >> 32);
  char topic[32];

  snprintf(topic, 32, "ESP-%04X/output", chip);

  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("ESP8266Client")) {
      Serial.println("connected");
      // Subscribe
      client.subscribe(topic);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void mqtt_Notifier(void *pvParameter) {
  double _Leq_dB;
  uint64_t chip_id = ESP.getEfuseMac();
  uint16_t chip = (uint16_t)(chip_id >> 32);
  UBaseType_t aantal;
  char ssid[17];
  char topic[32];
  char tmpString[128];

  snprintf(ssid, 17, "ESP-%04X%08X", chip, (uint32_t) chip_id);
  snprintf(topic, 32, "ESP-%04X/soundlevel", chip);
  Serial.println(ssid);

  while(xQueueReceive(samples_queue, &_Leq_dB, portMAX_DELAY)){
      aantal = uxQueueMessagesWaiting(samples_queue);
      // notify changed value
      String tempString = String(_Leq_dB);
      snprintf(tmpString, 32, "(%d) %s", int(aantal), tempString);
      Serial.println(tmpString);

      if (!client.connected()) {
        reconnect();
      }
      client.loop();

      long now = millis();
      if (now - lastMsg > 5000) {
        lastMsg = now;
        
        // Convert the value to a char array
        
        snprintf(tmpString, 8, "%s", tempString);
        // client.publish("esp32/soundlevel", tmpString);
        client.publish(topic, tmpString);
      }
      vTaskDelay(100 / portTICK_PERIOD_MS);
  }
}

void task_mqtt() {
  setup_wifi();
  xTaskCreate(mqtt_Notifier, "MQTT Notifier", MQTT_TASK_STACK, NULL, MQTT_TASK_PRI, NULL);
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
}

void toggleLED(void *pvParameter) {
  while(1) {
      digitalWrite(ledPin, HIGH);
      vTaskDelay(5000 / portTICK_PERIOD_MS);
      digitalWrite(ledPin, LOW);
      vTaskDelay(5000 / portTICK_PERIOD_MS);
  }
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(ledPin, OUTPUT);
  task_slm();
  vTaskDelay(500 / portTICK_PERIOD_MS);
  // delay(500);
  task_mqtt();
  xTaskCreatePinnedToCore(toggleLED, "Toggle LED", 1024, NULL, 1, NULL, 0);
}

void loop() {
  // put your main code here, to run repeatedly:
}